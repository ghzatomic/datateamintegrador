var DataTeamIntegrationService = require('./DataTeamIntegrationService');

class ServicesFactory{
    constructor(){
    }

    criaDataTeamIntegrationService(urlServidorDataTeam){
        if (this.dataTeamIntegrationService == undefined){
            this.dataTeamIntegrationService = new DataTeamIntegrationService(urlServidorDataTeam) ;
        }
        return this.dataTeamIntegrationService;
    }
}


module.exports = ServicesFactory;