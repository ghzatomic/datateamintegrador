const DataTeamIntegrationService = require("./services/DataTeamIntegrationService");
const ServicesFactory = require('./services/ServicesFactory');

const servicesFactory= new ServicesFactory();
const dataTeamIntegrationService = servicesFactory.criaDataTeamIntegrationService("") 

exports.instituicao = async event => {
  event.Records.forEach(record => {
    if (record.hasOwnProperty('Sns')) {
      const sns = record.Sns;
      if (sns.hasOwnProperty('Message') && sns.hasOwnProperty('TopicArn')) {
        const topicArn = sns.TopicArn.substring(sns.TopicArn.lastIndexOf(":")+1,sns.TopicArn.length);
        const message = sns.Message;
        dataTeamIntegrationService.enviaDados(topicArn,message);
      }
    }


  });
  return {
    'statusCode': 200
  };

};
